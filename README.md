# BusRoute

This is an Elixir implementation of https://github.com/goeuro/dev-test/tree/new-test
I work for GoEuro, and I thought it might be a fun and challenging ways to learn a new language =)

Provides a REST interface that calculates whether there is a direct route between two station ids.

When running locally, and in docker that is *not* inside VirtualBox, try: 
http://localhost:8088/api/direct?dep_sid=1&arr_sid=5

For those of you running docker inside VirtualBox, try:
http://192.168.99.100:8088/api/direct?dep_sid=1&arr_sid=5

## Running
`run --no-halt mix.exs data.txt`

## Running tests
`mix test --no-start`

## Building docker images
`./build.sh`

## Running the service
`./service.sh start data.txt`, where `data.txt` is the path to your route data file.

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `bus_route` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:bus_route, "~> 0.1.0"}]
    end
    ```

  2. Ensure `bus_route` is started before your application:

    ```elixir
    def application do
      [applications: [:bus_route]]
    end
    ```

