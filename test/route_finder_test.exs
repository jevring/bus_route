defmodule RouteFinderTest do
  use ExUnit.Case
  import RouteFinder
  # Can be used with specially formatted @moduledoc comments
  # doctest RouteLoader
  
  # setup_all means that it is only run once. use setup to run before each test
  setup_all do
    station_id_to_route_and_index = RouteLoader.load("test/test_data.txt")
    {:ok, [index: station_id_to_route_and_index]}
  end
  
    test "direct route between 1 and 3", %{index: station_id_to_route_and_index} do
      assert is_direct_route(station_id_to_route_and_index, 1, 3)
    end
    
    test "direct route between 0 and 4", %{index: station_id_to_route_and_index} do
      assert is_direct_route(station_id_to_route_and_index, 0, 4)
    end
    
    test "no direct route between 4 and 0", %{index: station_id_to_route_and_index} do
		assert not is_direct_route(station_id_to_route_and_index, 4, 0)
    end
    
    test "no direct route between 5 and 10", %{index: station_id_to_route_and_index} do
       # 10 doesn't exist in the data set
       assert not is_direct_route(station_id_to_route_and_index, 5, 10)
    end
  
end