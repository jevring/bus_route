defmodule BusRoute do
	use Application
	
  # run with: run --no-halt mix.exs data.txt	
  def start(_type, _args) do

    # todo: maybe we should have a fallback, or at least an intelligible error message if no data file was provided
    # todo: using System.argv() is fantastically error-prone, because it works so incredibly poorly when launched through mix
    data_file = List.first(System.argv());
	# http://elixir-lang.org/docs/stable/elixir/Application.html
	BusRouteSupervisor.Supervisor.start_link(data_file)
  end
end
