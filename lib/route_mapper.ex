defmodule RouteMapper do
  @moduledoc """
    Creates a reverse index from station id to a pair of route id and position in the route
    
    Map<StationId, Map<RouteId, StationIndexInRoute>>>
	"""
	
	def reverse_index(%Model.Routes{routes: routes}) do
		reverse_index(Map.new(), routes)
	end
	
	defp reverse_index(outer_map, [route | remaining_routes]) do
		outer_map = reverse_index_for_route(route, outer_map)
		reverse_index(outer_map, remaining_routes)
	end
	
	defp reverse_index(outer_map, []) do
		outer_map
	end
	
	defp reverse_index_for_route(%Model.Route{route_id: route_id, stops: stops}, outer_map) do
		map_stops(route_id, outer_map, 0, stops)
	end
	
	defp map_stops(route_id, outer_map, station_index, [stop | stops]) do
		# GAAAAAAAAAAH! 
		# I forgot that shit was immutable, and thus didn't re-assign the output back to outer_map!
		
		# First ensure that the inner map exists
		outer_map = Map.put_new_lazy(outer_map, stop, &Map.new/0)
		# Then write directly to it (this is a handy feature!)		
		outer_map = Kernel.put_in(outer_map, [stop, route_id], station_index)
		map_stops(route_id, outer_map, station_index + 1, stops)
	end
	
	defp map_stops(_, outer_map, _, []) do
		outer_map
	end
end