defmodule RouteFinderServer do
  @moduledoc false
  
  use GenServer

  def start_link(route_file) do
    # todo. should we name this "pid" in some better way? it's the service name, basically, but it's just used internally.
    GenServer.start_link(__MODULE__, route_file, name: RouteFinderServerPid)
  end

  def init(route_file) do
    IO.puts("Starting route finder server for file #{route_file}")
    station_id_to_route_and_index = RouteLoader.load(route_file)
    {:ok, station_id_to_route_and_index}
  end

  def handle_call([departure_station_id, arrival_station_id], _from, station_id_to_route_and_index) do
    direct = RouteFinder.is_direct_route(station_id_to_route_and_index, departure_station_id, arrival_station_id)
    {:reply, direct, station_id_to_route_and_index}
  end

  def handle_cast(_msg, station_id_to_route_and_index) do
    {:noreply, station_id_to_route_and_index}
  end
  
  def is_direct_route(departure_station_id, arrival_station_id) do
    GenServer.call(RouteFinderServerPid, [departure_station_id, arrival_station_id])
  end
end