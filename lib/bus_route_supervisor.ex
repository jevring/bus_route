defmodule BusRouteSupervisor.Supervisor do
  @moduledoc false
  
  use Supervisor

  def start_link(route_file) do
    Supervisor.start_link(__MODULE__, route_file)
  end

  def init(route_file) do
    # hehe, oops, do NOT add yourself as a supervisor to the children!
    # I thought maybe that was the trick to actually keep the fucker from immediately shutting down, 
    # but that completely froze my computer until erlang crashed.
    
    children = [
      worker(RouteFinderServer, [route_file], restart: :temporary),
      Plug.Adapters.Cowboy.child_spec(:http, Http.Api.Endpoints.ApiRouter, [], [port: 8088])
    ]

    supervise(children, strategy: :one_for_one)
  end
end