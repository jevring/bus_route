defmodule RouteFileReader do
  @moduledoc """
    Reads a route-file with the following format:
    number_of_routes
    route_id stop_id*
    route_id stop_id*
    route_id stop_id*
	"""
	
   @doc """
    Returns a %Routes struct
   """
   def read_route_file(filename) do
     #stream_of_lines = File.stream!(file, :utf8)
     #Enums.reduce(stream_of_lines, [], read_line)
     file = File.open!(filename, [:read, :utf8])
     
     # read a single line of the file
     
     
     number_of_routes = IO.read(file, :line) 
                        |> String.trim 
                        |> String.to_integer
     #IO.puts("Number of routes: #{number_of_routes}")
     
     routes = IO.stream(file, :line)
     |> Stream.map(&String.trim/1)
     |> Stream.map(&(String.split(&1, " ")))
     |> Stream.map(fn list -> Enum.map(list, &String.to_integer/1) end) # if we convert here, 
     |> Stream.map(&process_route/1)
     |> Enum.take(number_of_routes)
     |> Enum.to_list
      
     %Model.Routes{number_of_routes: number_of_routes, routes: routes} 
     
     #IO.stream(file, :line)
     #|> Stream.map(&wrap/1)
     #|> Stream.map(&IO.puts/1)
     #|> Enum.take(number_of_routes)
     
     #IO.read(file, :line)
     #|> wrap
     #|> IO.puts
   end
   
   #defp wrap(line) do
   #  "Read line: #{line}"
   #end
   
   defp process_route([route_id | stations]) do
     %Model.Route{route_id: route_id, stops: stations}
   end
end