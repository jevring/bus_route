defmodule RouteFinder do
  @moduledoc """
    There is a direct route between A and B iff:
	* A and B are stations that share at least one route
	* A comes *before* B in at least one shared route  
	"""
  
  def is_direct_route(reverse_index, departure_station, arrival_station) do
    # reverse_index is, more explicitly station_id_to_route_and_index
    departure_routes_and_station_index = Map.get(reverse_index, departure_station)
    arrival_routes_and_station_index = Map.get(reverse_index, arrival_station)
	is_direct_route(departure_routes_and_station_index, arrival_routes_and_station_index)    
  end
  
  defp is_direct_route(nil, _) do
	false      
  end
    
  defp is_direct_route(_, nil) do
    false      
  end

  defp is_direct_route(nil, nil) do
  	false      
  end


  
  @docp """
    Ok, the stations both exist in some routes. Let's see if they have any overlapping routes.
	"""
  defp is_direct_route(departure_routes_and_station_index, arrival_routes_and_station_index) do
       departure_route_ids = set_of_keys(departure_routes_and_station_index)
       arrival_route_ids = set_of_keys(arrival_routes_and_station_index)
       shared_route_ids = MapSet.intersection(departure_route_ids, arrival_route_ids) |> MapSet.to_list
	   # Now that we have a list of the routes where both stations appear, 
	   # we have to check if the index of the departure is LOWER than the index of the arrival
	   departure_before_arrival = fn route_id -> departure_before_arrival_in_route(route_id, departure_routes_and_station_index, arrival_routes_and_station_index) end 	       
	   Enum.filter(shared_route_ids, departure_before_arrival) 
	   |> Enum.any? 	       
  end
  
  defp set_of_keys(map) do
    # todo: which, if any, of these solutions is idiomatic elixir?
    #MapSet.new(Map.keys(map))
    map |> Map.keys |> MapSet.new
  end

  defp departure_before_arrival_in_route(route, departure_routes_and_station_index, arrival_routes_and_station_index) do
    departure_station_index_for_route = Map.get(departure_routes_and_station_index, route)
    arrival_station_index_for_route = Map.get(arrival_routes_and_station_index, route)
    departure_station_index_for_route < arrival_station_index_for_route
  end	
end