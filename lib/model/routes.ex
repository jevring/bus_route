defmodule Model.Routes do
  @moduledoc false
  defstruct number_of_routes: 0, routes: []
end