defmodule Http.Api.Endpoints.HelloWorldController do
  @moduledoc """
    Plug handler that handles /hello/world requests
	"""
	
	def init(default_opts) do
	  default_opts
	end
	
	def call(connection, _options) do
	  # https://codewords.recurse.com/issues/five/building-a-web-framework-from-scratch-in-elixir
	  map_request(connection.method, connection.path_info, connection)
	end
	
	# /hello is already matched in ApiRouter
	defp map_request("GET", ["world"], connection) do
	  connection |> Plug.Conn.put_resp_content_type("text/plain") |> Plug.Conn.send_resp(200, "Hello world")
	end
	
#	defp map_request("GET", _, connection) do
#      connection |> Plug.Conn.put_resp_content_type("text/plain") |> Plug.Conn.send_resp(404, "XXX Not found")	  
#    end
#	
#	defp map_request(_, _, connection) do
#	  connection |> Plug.Conn.put_resp_content_type("text/plain") |> Plug.Conn.send_resp(405, "XXX Method not allowed")	  
#	end
  
end