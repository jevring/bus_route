defmodule Http.Api.Endpoints.DirectRouteController do
  @moduledoc false
  use Plug.Router
  
  # NOTE: The order here is VERY important.
  # If the order between :match and :dispatch is changed, 
  # the connection will simply hang forever and do nothing.
  # Not very user friendly.
  # If we omit :dispatch, we get a super weird error instead
  plug :match
  plug :dispatch
  
    
  get "/direct" do
    # todo: some error handling
    conn = fetch_query_params(conn)
    %{"dep_sid" => departure_station, "arr_sid" => arrival_station} = conn.params
    # Man, Elixir, fuck you and your dynamic typing!
    # These things were string all along, and there were no complaints.
    # Of course shit wasn't going to work!
    departure_station = String.to_integer(departure_station)
    arrival_station = String.to_integer(arrival_station)
    direct = RouteFinderServer.is_direct_route(departure_station, arrival_station)
    json = Poison.encode!(%JSON.DirectRouteResponse{dep_sid: departure_station, arr_sid: arrival_station, direct_bus_route: direct})
    #IO.puts("Departure: #{departure_station}, Arrival: #{arrival_station}, JSON: #{json}")
    conn 
    |> put_resp_content_type("application/json") 
    |> send_resp(200, json)
  end
  
  # we have to have this, otherwise plug flips its shit when we run in to an unknown mapping 
  match _ do
    send_resp(conn, 404, "Not found (Http.Api.Endpoints.DirectRouteController)")
  end
end