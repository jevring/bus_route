defmodule Http.Api.Endpoints.ApiRouter do
  @moduledoc false
  use Plug.Router
  
  plug Plug.Logger
  # define a catch-all that does something for whatever else wasn't matched.
  # This points to the "match" directive below
  plug :match
  plug :dispatch

  # https://hexdocs.pm/plug/Plug.Router.html
  
  # Note, the part that is matched here is DROPPED when forwarding.
  # For example, /api/direct, forwarded to Http.Api.Endpoints.DirectRouteController, 
  # means that Http.Api.Endpoints.DirectRouteController needs to match /direct only!
  forward "/api", to: Http.Api.Endpoints.DirectRouteController
  forward "/hello", to: Http.Api.Endpoints.HelloWorldController
  
  match _ do
    send_resp(conn, 404, "Not found (ApiRouter)")
  end

  
end