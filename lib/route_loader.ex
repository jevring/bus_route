defmodule RouteLoader do
  @moduledoc false
  def load(route_file) do
    
    IO.puts("Loading routes from file: #{route_file}")
    routes = RouteFileReader.read_route_file(route_file)
    IO.puts("Routes")
    IO.inspect(routes)
    station_id_to_route_and_index = RouteMapper.reverse_index(routes)
    IO.puts("Reverse index")
    IO.inspect(station_id_to_route_and_index)
  end
end