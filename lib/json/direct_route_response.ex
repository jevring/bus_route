defmodule JSON.DirectRouteResponse do
  @moduledoc """
        This struct represents the JSON structured response.
	"""
  @derive [Poison.Encoder]
  defstruct [:dep_sid, :arr_sid, :direct_bus_route]
end