FROM elixir:1.3.4

RUN mkdir /elixir
COPY config /elixir/config
COPY lib /elixir/lib
COPY mix.exs /elixir/mix.exs
COPY data.txt /elixir/data.txt


WORKDIR /elixir
# todo: cd to the right directory, copy in the actual elixir app (how do we package?)
RUN mix local.hex  --force
RUN mix deps.get
RUN mix deps.compile
RUN mix compile
# todo: get the file in via something
# todo: how can we get syntax higlighting in docker files?
CMD mix run --no-halt mix.exs /elixir/route_data.txt