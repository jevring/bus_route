defmodule BusRoute.Mixfile do
  use Mix.Project

  def project do
    [app: :bus_route,
     version: "0.1.0",
     elixir: "~> 1.3",
     # escript: [main_module: CLI], # with this, we can do "mix escript.build" to get an executable.
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps()]
  end

  # Configuration for the OTP application
  #
  # Type "mix help compile.app" for more information
  def application do
    # If we want to store the build-time arguments, we can do this: mod: {BusRoute, System.argv()},
    # and then, in start/2, we can get the value from args
    [mod: {BusRoute, []}, 
     applications: [:logger, :cowboy, :plug]
    ]
  end

  # Dependencies can be Hex packages:
  #
  #   {:mydep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:mydep, git: "https://github.com/elixir-lang/mydep.git", tag: "0.1.0"}
  #
  # Type "mix help deps" for more examples and options
  defp deps do
    [{:poison, "~> 3.0"},
     {:cowboy, "~> 1.0.0"},
     {:plug, "~> 1.0"}]
  end
end
