#!/bin/bash
CONTAINER_FILE="/tmp/bus_route_container.pid"
ACTION=$1

ROUTE_DATA_FILE=$2
if [ -n "$ROUTE_DATA_FILE" ]; then
	ROUTE_DATA_FILE=$(realpath $ROUTE_DATA_FILE)
	if [ $TERM == 'cygwin' ]; then
		ROUTE_DATA_FILE=$(cygpath -a $ROUTE_DATA_FILE)
		ROUTE_DATA_FILE=${ROUTE_DATA_FILE:9}
	fi
fi	

function start() {
	# https://linuxconfig.org/how-to-start-a-docker-container-as-daemon-process
	
	# this works! 
	# LOWER CAST drive name.
	#docker run --rm -v /c/Users/Markus/Documents/code/elixir/bus_route/data.txt:/elixir/route_data.txt -p 8 088:8080 -it bus_route
 	CONTAINER=$(docker run -d -v $ROUTE_DATA_FILE:/elixir/route_data.txt -p 8088:8088 -t bus_route)
 	echo $CONTAINER > $CONTAINER_FILE
}

function stop() {
	if [ -e $CONTAINER_FILE ]; then
		CONTAINER=$(cat $CONTAINER_FILE)
		RUNNING=$(docker inspect -f {{.State.Running}} $CONTAINER)
		if [ $RUNNING ]; then
		    # else it's already been stopped
			docker kill $CONTAINER
		fi
	else
		echo "Could not find $CONTAINER_FILE. Can't identify running container"
	fi
	rm -f $CONTAINER_FILE
}

function block() {
	docker run --rm -v $ROUTE_DATA_FILE:/elixir/route_data.txt -p 8088:8088 -it bus_route	 
}

case "$ACTION" in
  start)
    start
    ;;
  stop)
    stop
    ;;
  block)
    block
    ;;
  *)
    echo "Usage: $0 {start|stop|block} DATA_FILE"
esac